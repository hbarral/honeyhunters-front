import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    comments: []
  },
  getters: {
    getComments (state) {
      return state.comments
    }
  },
  mutations: {
    addComment (state, comment) {
      state.comments.push(comment)
    },
    addComments (state, comments) {
      state.comments = comments
    }
  },
  actions: {
    createComment (context, formData) {
      return new Promise((resolve, reject) => {
        let url = 'api/comment'

        Vue.axios.post(url, formData)
          .then(response => {
            let comment = response.data.comment
            console.log(comment)

            context.commit('addComment', comment)
            resolve(response)
          })
          .catch(error => {
          // console.log(error)
            reject(error)
          })
      })
    },
    getComments (context) {
      let url = 'api/comment'

      Vue.axios.get(url)
        .then(response => {
          let comments = response.data.comments
          context.commit('addComments', comments)
        })
        .catch(error => {
          // console.log(error)
        })
    }

  }
})
