import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faVk, faFacebook, faYoutube } from '@fortawesome/free-brands-svg-icons'
import VeeValidate, { Validator } from 'vee-validate'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ru from '../node_modules/vee-validate/dist/locale/ru.js'

library.add(faFacebook, faVk, faYoutube)

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Validator.localize('ru', ru)
Vue.use(VeeValidate, {
  locale: 'ru'
})
Vue.use(VueAxios, axios)

// Axios defaults
axios.defaults.baseURL = 'https://desolate-headland-82333.herokuapp.com'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
